﻿CREATE TABLE [dbo].[AddressNew] (
  [ID] [int] NOT NULL,
  [AddressLine] [nvarchar](50) NULL,
  [City] [nvarchar](30) NULL,
  [PostalCode] [nvarchar](15) NULL
)
ON [PRIMARY]
GO